# coding: utf-8
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from codecs import getwriter
import json
import requests
from requests.adapters import HTTPAdapter
from signal import signal, SIGINT, SIGTERM
from sys import stdout, exit, exc_info, stderr
import threading
import Queue

TIMEOUT = 90
MAX_RETRIES = 20
ALIVE = True
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}


def clean(val):
    return val.strip()


class Page(object):

    def __init__(self, page_url, **kwargs):
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(page_url, HTTPAdapter(max_retries=MAX_RETRIES))
        self._url = page_url
        self._category = kwargs.get('title')

    def _get_page_rows(self):
        response = self._session.get(self._url)
        soup = BeautifulSoup(response.content, 'html.parser')
        return soup.find('div', attrs={'id': 'content-inner'}).find('form').find('table').findAll('tr')

    def get_result(self):
        rows = self._get_page_rows()
        result = []
        for row in rows:
            if not row.find('td', attrs={'align': 'center', 'class': 'row4'}):
                continue
            td_list = row.findAll('td')
            data = {
                'url': clean(td_list[2].find('div').find('a').get('href')),
                'author': {  
                    'id': clean(td_list[3].find('a').get('href')),
                    'login': clean(td_list[3].find('a').text)
                },
                'title': clean(td_list[2].find('div').find('a').text),
                'answers': clean(td_list[4].text),
                'views': clean(td_list[5].text),
                'rating': clean(td_list[-2].find('div').text),
                'category': clean(u'Лента/%s'%self._category),
            }
            description = td_list[2].find('div', attrs={'class': 'desc'})
            if description:
                data['description'] = clean(description.text)
            result.append(data)
        return result


class Record(object):
    THREADS = 12
    ON_PAGE = 25

    def __init__(self, record, **kwargs):
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(record['url'], HTTPAdapter(max_retries=MAX_RETRIES))
        self._record = record
        self._type = kwargs.get('type', 'img')

    def get_comment(self, html):
        data = {
            'id': clean(html.findAll('tr')[0].findAll('td')[0].findAll('a')[0].get('name')),
            'author': {  
                'id': clean(html.find('a', attrs={'title': u'Профиль'}).get('href')),
                'login': clean(html.findAll('tr')[0].findAll('td')[0].find('span', attrs={'class': 'normalname'}).text),
                'status': clean(html.findAll('tr')[1].findAll('td')[0].text.split('\r\n')[1]),
                'registration': clean(html.findAll('tr')[1].findAll('td')[0].findAll('div')[0].text.split('\r\n')[1].replace(u'Регистрация:', '')),
                'number_comments': clean(html.findAll('tr')[1].findAll('td')[0].findAll('div')[0].text.split('\r\n')[-1].replace(u'Сообщений:', '')),
            },
            'date': clean(html.findAll('tr')[0].findAll('td')[1].findAll('div')[0].find('a').text),
        }
        if html.find('a', attrs={'target': '_blank'}):
            data['author']['awards'] = [clean(html.find('a', attrs={'target': '_blank'}).text)]
        data['content'] = [{
            'type': 'str',
            'value': clean(html.findAll('tr')[1].findAll('td')[1].find('div', attrs={'class': 'postcolor'}).text)
        }]
        try:
            if html.findAll('tr')[1].findAll('td')[1].find('div', attrs={'style': 'width:100%;overflow-x:hidden;'}).find('img'):
                data['content'].append({
                    'type': 'img',
                    'value': clean(html.findAll('tr')[1].findAll('td')[1].find('div', attrs={'style': 'width:100%;overflow-x:hidden;'}).find('img').get('src'))    
                })
        except:
            pass
        return data

    def get_comments(self, html):
        result = []
        for table in html.findAll('table', attrs={'width': '100%', 'border': '0', 'cellspacing': '1', 'cellpadding': '3'}):
            try:
                if not table.get('id') and table.get('id').find('p_row') < 0:
                    continue
                result.append(self.get_comment(table))
            except Exception as e:
                pass
        return result

    def get_url(self, i):
        return '/'.join(self._record['url'].split('/')[:-1])+('/st/%s/'%(self.ON_PAGE*i))+self._record['url'].split('/')[-1]

    def get_result(self):
        response = self._session.get(self._record['url'])
        soup = BeautifulSoup(response.content, 'html.parser')
        a = next((a for a in soup.find('div', attrs={'id': 'content-inner'}).findAll('table')[0].find('table', attrs={'class': 'tableborder'}).findAll('a') if a.get('title') and a.get('title').find(u'Страница:') > -1), None)
        if not a:
            return []
        page_count = int(a.get('title').replace(u'Страница: ', ''))
        comments = []
        pages = []
        for i in xrange(page_count):
            if i == 0:
                comments += self.get_comments(soup)
            else:
                pages.append(self.get_url(i))

        threads = []
        work_q = Queue.Queue()
        raw_data = Queue.Queue()
        for page in pages:
            work_q.put(page)

        def mult_process(work_q, raw_data):
            while ALIVE:
                if work_q.empty():
                    break
                page = work_q.get()
                try:
                    response = self._session.get(page)
                    soup = BeautifulSoup(response.content, 'html.parser')
                    raw_data.put(self.get_comments(soup))
                except Exception as e:
                    pass

        thread_count = self.THREADS if self.THREADS >= len(pages) else len(pages)
        for x in range(thread_count):
            threads.append(threading.Thread(target=mult_process, args=(work_q, raw_data, )))
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        result = []
        while True:
            if raw_data.empty():
                break
            comments += raw_data.get_nowait()
        return comments


class Client(object):
    THREADS = 24
    ON_PAGE = 30

    def __init__(self, sout, BASE):
        self._BASE = BASE
        self.sout = sout
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(self._BASE['url'], HTTPAdapter(max_retries=MAX_RETRIES))
        self._queue = []
        response = self._session.get(self._BASE['url'])
        soup = BeautifulSoup(response.content, 'html.parser')
        for page_ind in xrange(int(soup.find('div', attrs={'id': 'content-inner'}).findAll('table')[0].findAll('a')[-1].get('title').replace(u'Страница: ', ''))):
            if page_ind == 0:
                self._queue.append(self._BASE['url'])
            else:
                self._queue.append(self._BASE['url']+'st/%s/100/Z-A/last_post'%(page_ind*30))

    def _run_pages(self, queue):
        threads = []
        work_q = Queue.Queue()
        raw_data = Queue.Queue()
        for page_url in queue[:1]:
            work_q.put(page_url)

        def mult_process(work_q, raw_data):
            while ALIVE:
                if work_q.empty():
                    break
                page_url = work_q.get()
                try:
                    res = Page(page_url, **self._BASE).get_result()
                    for record in res:
                        raw_data.put(record)
                except Exception as e:
                    pass

        for x in range(self.THREADS):
            threads.append(threading.Thread(target=mult_process, args=(work_q, raw_data, )))
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        return raw_data

    def _run_records(self, work_q):
        threads = []
        raw_data = Queue.Queue()

        def mult_process(work_q, sout):
            while ALIVE:
                if work_q.empty():
                    break
                record = work_q.get()
                try:
                    record['comments'] = Record(record, **self._BASE).get_result()
                except Exception as e:
                    pass
                sout.write(json.dumps(record, ensure_ascii=False) + "\n")

        for x in range(self.THREADS/4):
            threads.append(threading.Thread(target=mult_process, args=(work_q, self.sout, )))
        for t in threads:
            t.start()
        for t in threads:
            t.join()

    def process(self):
        records = self._run_pages(self._queue)
        self._run_records(records)

def read_key():
    argp = ArgumentParser()
    argp.add_argument('--type', type=str, required=True, help="Type")
    args = argp.parse_args()
    return args.type


def main():
    sout = getwriter("utf8")(stdout) 
    serr = getwriter("utf8")(stderr)
    URLS = {
        'photos': {
            'url': 'http://www.yaplakal.com/forum2/',
            'title': u'Картинки',
            'type': 'img',
        },
        'videos': {
            'url': 'http://www.yaplakal.com/forum28/',
            'title': u'Видео',
            'type': 'video',
        },
        'photojab': {
            'url': 'http://www.yaplakal.com/forum27/',
            'title': u'Фотожаба',
            'type': 'img',
        }
    }
    dump_type = read_key()
    client = Client(sout, URLS[dump_type])
    
    try:
        client.process()
    except Exception as e:
        exc_traceback = exc_info()[2]
        filename = line = None
        while exc_traceback is not None:
            f = exc_traceback.tb_frame
            line = exc_traceback.tb_lineno
            filename = f.f_code.co_filename
            exc_traceback = exc_traceback.tb_next
        serr.write(json.dumps({
            'error': True,
            'details': {
                'message': str(e),
                'file': filename,
                'line': line
            }
        }, ensure_ascii=False) + "\n")


if __name__ == "__main__": 
    def signal_handler(signal, frame):
        global ALIVE
        ALIVE = False
        exit(0)
    signal(SIGINT, signal_handler)
    signal(SIGTERM, signal_handler)
    main() 
